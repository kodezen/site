---
title: How To Focus On Reading
description: This is a guide on how to focus on reading
date: "2019-07-25T22:12:03.284Z"
---

Remember when you were a kid and you could spend hours lost in a book? Turning page after page without noticing the passage of time, and being startled to look up and see that it's dark outside. For some of you this might be a fond memory that you can relate too. For others, a younger audience, it might be an experience that you’ve never experienced but yearn too.

![Reading](./reading-opener.jpg)

In either case, you might be wondering how to be able to read deeply and focus while reading. It can seem like there’s a sense of mystery around the process. You might be thinking why can't I read deeply anymore? Is it just because I’ve gotten older? Have I developed ADD?

And perhaps what bothers you the most is not just that you can’t focus on reading, but that you don’t love the process. That books seem dry and tedious, something you have to force yourself to do like the people at the gym pushing themselves to exhaustion on the treadmill. 

If you felt like this before don't worry. You can read deeply again. You can love reading again. But first you must understand why this is happening. The reason is simple:

### *The internet has short circuited your brain.*

Modern neuroscience has shown that the influx of short term stimulation and noise from the internet can destroy our ability to focus. This means that if you spent a lot of time over the years browsing Facebook, YouTube, Twitter etc. your brain's ability for deep focus might be ruined.

Thankfully, your brain can rapidly recover within just a few weeks so that you can read for hours again. In the following steps below I’ve outlined exactly how to do it:


###Step 1: Set Aside One Hour For Reading

According to research done on the human brain, it takes your brain about 25 minutes to get into it’s ‘Peak Focus Mode’ where it's operating at 100% performance. This means that if you read for short intervals here and there, your brain never gets a chance to enter it's 'Peak Focus Mode' where its at it's strongest capability. So set aside at least 1 hour to read at a time perhaps in the evening or in the morning, rather than trying to read for 15 minutes here and there.

After doing this you still might have trouble focusing for more than 15-20 minutes without feeling like you need a break or want to check something. This is because there's still a lot of repairing left to do. You see it's not just what you do while reading that matters, what you do in the time your not reading matters too.

If it helps you can think of it a bit like sports. If you were a tennis player you have to train hard in practice but you also have to maintain a discilpined life outside of practice. You can't eat McDonald's and smoke cigarettes the second you step off the court and expect to perform well. That would be throwing away all the benefits that you worked so hard for.

You have to think the same way. What you do outside the time you spend actually reading matters. If for the rest of your day you’re multitasking, surfing the web aimlessly, your brain won't get the chance it needs to recover.

This means there are two more steps:

* Set up your digital environment for focus.
* Practice focusing on one thing at a time.

###Step 2: Set Up Your Computer For Focus

Our computers are the number one reason our brain's ability to read gets weakened. Thankfully it’s easy to turn our computer into a tool for focus and productivity rather than a portal into a world of clickbait, cat videos, and memes. I reccomend using four tools for this.

* ###Kora

You can use [Kora](https://trykora.com) to set up blocks to reduce the time spent surfing distracting sites like YouTube, Twitter, or Facebook that can affect your focus. 

![30min](./30min.png)
![blocklength](./blocklength.png)

You can set up schedules like this so you can carve out periods of focus, where your brain gets a break the noise of the internet so it can get a chance to recover and regain it’s full strength.

![Blocks](./Blocks.png)
![MondayFriday](./MondayFriday.png)

* ###Instapaper

You can use [Instapaper](https://www.instapaper.com/) to read everything you find online in a clean and distraction free environment. 

* ### uBlockOrigin

Use an adblocker like [uBlockOrigin](https://chrome.google.com/webstore/detail/ublock-origin/cjpalhdlnbpafiamejdnhcphjbkeiagm?hl=en) so that you don’t get bombarded by ads 24/7.

* ###xTab

Use [xTab](https://chrome.google.com/webstore/detail/xtab/amddgdnlkmohapieeekfknakgdnpbleb?hl=en) to limit the number of tabs you have open to a reasonable number, I usually set it to a maximum of 5 tabs open at any given time.

### Step 3: FOOT (Focus On One Thing)

To top this all off and develop your ability to focus to be as strong as possible you need to start focusing on doing one thing at a time.

For the next few days count how many things you’re doing at the same time throughout the day. Check emails, shop on Amazon, while typing up a report? Watch tv, scroll through instagram, while listening to Spotify? Eat breakfast, watch the news, and respond to texts?

Multitasking like this scatters your brain’s focus so that it gets weaker and weaker. When you start to do the opposite and focus your attention on one point it, your brain turns into a laser that can scorch through tasks.

![Alexander Graham Bell](./Alexander_Graham_Bell.jpg)

In the words of Alexander Graham Bell, the inventor of the telephone you should:

“Concentrate all your thoughts upon the work at hand. The sun's rays do not burn until brought to a focus.”

Start to pare down the number of things your brain is juggling in any given moment to just one thing.  In the morning focus on just brushing your teeth, then drinking your coffee, then getting in the car and driving to work.  When at work focus on just responding to email, then finish and move on to writing your report, then take a break and just focus on eating lunch. Sit at the table and just eat dinner with your family, don't read, use your laptop, or watch TV.

When you focus on things one by one your mind strengthens. You’ll finish your work a lot faster because this is how your brain was designed to operate. For peak 100% functioning your mind has to be focused on one thing.

This will pay dividends because the same muscles you use for focusing everywhere else, you use to focus on reading.  That’s why practicing deep focus in other areas of your life, will translate to better focus while reading too. The longer you do this the faster your brain will recover and the stronger it will get. After a few weeks you'll start to notice that you're able to read for much longer than you used to be able to. Even more importantly, you'll feel a spark of excitement that goes off when you think about reading. You'll feel a sensation of deep joy while reading and will want to read for hours. These are signs that your brain is strengthening and recovering from the noise of the internet. 

Remember that it’s up to you to maintain these benefits. You have to maintain your discipline to keep your brain’s ability to focus strong. If you stop lifting weights, eating nutritious food, and sleeping well your body would slowly lose muscle and get flabby again.
Likewise if you go back to continuous distraction from your phone or computer your brains focus muscle will grow weak again. For best results keep distractions minimal and keep your focus on one thing at a time. 

Good luck and happy reading.