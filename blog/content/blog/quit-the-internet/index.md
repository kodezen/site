---
title: Why you shouldn't quit the internet
description: This is a guide on why you shouldn't quit the internet
date: "2019-08-16T22:12:03.284Z"
---

If you're reading this chances are you relationship with the internet is... 

... *complicated.*

Maybe you're spending too much time on social media. Maybe you use the internet as a form of escape and procrastination. Or maybe you're feeling overwhelmed and want a break from it all. 

I've felt all of these things and more at different stages of my jourey into the digital world. At one point I even identified as an internet addict. 

I would waste upwards of 10 hours a day online and feel awful about it. **And I believed quitting the internet was the key to it all.**

![internet_addict](./internet_addict.jpg)

It was the ultimate solution. I envisioned a life without the internet and it seemed like paradise. The answer to all my life's problems and struggles.

**However, this line of thinking is flawed, disempowering, and not a reliable road to happiness and fulfillment.**

How do I know this? From experience. 

Today I no longer identify as an internet addict. In fact I've done a complete 180. Today I use the internet *only* for productive pursuits. 

**I've gone from wasting up to 10 hours a day online to using the internet as an empowering tool to help me reach my dreams in life.**

In this post I'm going share important insights that helped me:
* Realize the internet wasn't the problem
* Realize quitting the internet wasn't a solution 
* Learn how to use the internet as a tool to improve my life

These insights have made all the difference for me and my hope is that they will help you on your own journey of learning to use the internet in an empowering way.

## 1. Quitting the internet is not the answer
One of the first key realizations I had was that **the internet was not the cause of my problems.** If other people were having fantastic experiences online, then logically, the internet itself couldn't be the root cause. 

Initially I faced this realization with denial and rationalization. I believed: 

"Oh well some other people can handle the internet, but I can't." 

That's an example of fixed mindset thinking and something I adjusted with time into the belief: 

"I'm not using the internet responsibly *yet* but I can learn how to."

This belief is a perfect example of **growth mindset** thinking and I highly recommend checking out a book called **[Growth Mindset](https://www.amazon.com/Mindset-Psychology-Carol-S-Dweck/dp/0345472322)** by Dr. Carolyn Dweck. It will be invaluable on your journey as you've likely built up lots of limiting beliefs over time regarding what's possible for you and your life.

<iframe width="560" height="315" src="https://www.youtube.com/embed/hiiEeMN7vbQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

**When I realized that internet was not the root cause, I started to understand that all the work that had to be done was within.** That the outward change in my experience of life would caused by and be a reflection of the change I was performing inwardly. One of my favorite quotes sums this up perfectly: 

> **"Nothing will change unless you change" - Jim Rohn**

![grow](./grow.jpg)

## 2. Don't rely on willpower
This goes completely against how we intuitively think about our goals. We try to face all of them and any obstacles along the way with willpower head-on. The problem with this approach is that willpower is finicky. 

**It's a finite resource and is not a reliable strategy for long term change.**

Trying to change your relationship with the internet through sheer force while changing nothing else about your life doesn't work. I know because all of my early failed attempts relied on this approach.

**Making lasting change requires an entirely different way of thinking. One that takes into account systems, tools, and environment.**

I'll write more on the above later in the post, but one strategy that works extremely well for changing your relationship with the internet and doesn't rely on willpower is using website and app blocking software.

Using these tools gives you the mental space and freedom from the undesirable parts of the internet you waste a lot of time on and lets you begin the process of change on a high note, full of hope and possbility. 

For desktop: My passion has been creating the world's best productivity tool for website blocking called **[Kora](https://trykora.com)**. **If you want to block time-wasting websites, start developing better internet habits, and be more productive, I highly recommend trying it out.**

[![Try Kora free](./kora_banner.png)](https://trykora.com)

**[Try Kora free](https://trykora.com)**

For mobile: I'd recommend checking out [Screen Time](https://support.apple.com/en-us/HT208982) (iOS) and [Digital Wellbeing](https://play.google.com/store/apps/details?id=com.google.android.apps.wellbeing&hl=en_US)  (Android).

## 3. Don't quit the internet because the internet is not bad
This is a big one. If you're struggling with your internet use chances are you're wearing jaded glasses that cause you to see the internet in an overly negative light. 

You start to group all your negative experiences and all the time wasting sites under the umbrella term "internet." And thus begins the the formation of a warped view of reality. 

**The truth is that the internet is not inherently a bad place. Your experience of the internet is entirely dependent on your use of it.**

For lots of people, the internet is an *incredible* place. They're using it to learn amazing things and grow as human beings. Anyone in the world with an internet connection can instantly start learning how to code, paint, dance, play an instrument, shoot a basketball, etc. 

This is the first time in history that amazing information like this is available and so readily accessible for everyone on the planet. As much as you might wish at times that you were born in the 1700's, **the truth is that all of us are extremely lucky to be alive at this time in history.**

![together](./together.jpg)

## 4. Your internet use is shaped by the rest of your life
Do you have empowering beliefs about youself? Do you have a passion? Or a hobby? 

Do you have a social life? Or any activities you like to do? 

Do you get out of the house regulary and do fun things?

**I noticed a direct correlation between the answers to these questions and my relationship to the internet at different stages in my life.** When my internet use was most disempowering is when the majority of my answers to the questions above were "No."

Now the majority of my answers to the questions above are "Yes" and my internet use is responsible and empowering. 

This is no coincidence. When our answers to these questions are in the negative, we turn to the internet to fill the void of meaning and fulfillment lacking in our lives. It offers us connection, novelty, sensuality, and entertainment. 

But it's an endeavor destined to fail from the beginning. **Meaning and fulfillment are not found in flashing pixels on computer screens.**

![failure](./failure.jpg)

Your focus should not be on quitting the internet. It should be on the pusuits and activities that will change the answers to the questions above from negative to positive.

It's true that at times our internet habits can trap us in a vicious cycle and make it hard to make the necessary life changes to improve them.

Fortunately there's a middle way. A much healthier alternative to the far too *black-and-white* thinking of wanting to quit the internet altogether. *Disconnecting.*

## 5. The middle way: disconnecting

If you're feeling overwhelmed as a result of your iternet use or if you're committed to making changes in your life, disconnecting from the internet for a period of time can be an effective strategy to provide mental clarity and the ability to focus on your goals.

It's important to note that this is entirely different than the *quit the internet* mentality. **This is using a brief period of disconnecting from the internet to improve your relationship with it. This is not giving it up altogether and giving up on any hope of ever using it responsibly.**

Lots of people have gone on small internet disconnects and have experienced very positive results. It's a great way to put some distance yourself and negative emotions stemming from compulsive internet use so that you can reflect on what you want out of life, make new distinctions, and make better decisions.

![disconnecting](./disconnecting.jpg)

If the disconnecting strategy sounds exciting to you, I'd highly recommend incorporating the following 2 insights into the time you spend away from the internet.

## 6. Read like your life depends on it

Because it does. The quality of your life is determined by the quality of your thoughts and the questions you ask yourself on a regular basis.

**Which means that your success with establishing better internet habits depends directly on your habitual thoughts.** Given that they're so important, it would be wise to take a moment to understand where our thoughts come from and how we can improve them. 

You can think of the thoughts and questions that you habitually ask yourself as the output of a system or machine. Every person has an internal machine that is spitting out these thoughts, and the quality of each persons thoughts are different. 

![machine](./machine.jpg)

Some people's machines spit out extremely empowering thoughts and they ask themselves empowering questions. Other people's machines spit out extremely disempowering thoughts and they ask themselves disempowering questions.

What accounts for the difference? Well lots of things actually, but the most important ones are your **beliefs, previous experiences, and environment**.

Unfortunately these inputs typically tend to stay the same if left to their own accord.

Which means that short of having some near-death experience, tragic accident, or spontaneous enlightenment, the truth is that the kinds of thoughts you're going to be having a year from now won't be very different from the kinds of thoughts you're having today.

Which means, by extension, that your life isn't going to be much different one year from now than it is today. Just more of the same.

Unless.. and this is a big unless...

You start to introduce new inputs into the system.

The best way I've found to do that is **through reading empowering books and listening to inspirational speakers on audio.** 

![reading](./reading.jpg)

Some of my most life changing insights about how to develop a better relationship with the internet came while I was reading the **[The Shallows](https://www.amazon.com/Shallows-What-Internet-Doing-Brains/dp/0393339750)** by Nicholas Carr. And some of my most life changing insights about what could be possible for my life came from reading **["Awaken the Giant Within"](https://www.amazon.com/Awaken-Giant-Within-Immediate-Emotional/dp/0671791540)** by Tony Robbins and listening to his audio tapes. 

The simple habit of absorbing empowering books and speakers has done more for my life than any other. **When you read and listen to empowering messages your beliefs about yourself naturally begin to change.** This leads to a change in the habitual thoughts you have on a regular basis. And as a result, you  become more likely to take different actions that you did before. This leads to a new set of experiences. 

**All of this feeds back into the machine and positvely affects the next set of outputs in a compounding manner.**

Reading and listening to empowering messages is the most effective tool I've found for kick starting this process and improving your life. I sincerely hope you give it an honest try and see what it can do for you.

<iframe width="560" height="315" src="https://www.youtube.com/embed/zTNNKUBDNsU?start=142" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## 7. Focus on feeding your mind instead of quitting the internet

**You have the most powerful tool in the universe at your disposal: your own human mind.** It can take you to heights you never dreamed were possible, and all it asks for in exchange is an investment of time and effort so that it can get better.

In the same way you work out your physical body, you must train in the mental arena. Feeding your mind is the most important thing you'll ever do and will be the difference between a life of happiness and sucess and one of regret and unrealized potential. 

The work you do in this area will take care of everything else. **Viewing your internet habits as the *effect* of your internal thoughts, beliefs, experiences and life situation makes everything so much simpler.**

You do not have to expend an ounce of willpower in forcing change in your behaviors. Improvements will come naturally if you devote 100% of your attention on improving the cause rather than the effect. 

![success](./success.jpg)

## Conclusion

This concludes this post on why you shouldn't quit the internet. I hope my thoughts and experiences filled you with new ideas to ponder on your journey of establishing better internet habits. Maybe you'll even use the new distinctions you've made to improve your life and realize your potential. That would make me very happy. 

Thank you for reading, it's been a pleasure.


