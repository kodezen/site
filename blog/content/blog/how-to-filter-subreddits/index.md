---
title: How to Filter Subreddits From /r/all
description: This is a guide how to filter subreddits
date: "2019-07-26T22:12:03.284Z"
---

Are you tired of seeing subreddits:

*you find annoying?*

*you waste a lot of time on?*

*contain movie and show spoilers?*

Fortunately getting them to no longer show up in your /r/all feed is **easy and straight-forward.** However there are a few gotchya's you should know about. 

The reddit redesign brought with it some subreddit filtering changes which has left a lot of users confused. 

**Our goal in making this guide was to make the filteirng process as clear as possible.** Below you'll find step-by-step walk-throughs for all of the different ways to filter these subreddits out on the reddit desktop site and native apps. 

## Let's get started!

---
**IMPORTANT**: please note that filtering only hides the subreddits from appearing in the /r/all feed. It does not **block** you from going on the subreddits you filter. 

**If you want to block distracting subreddits:** check out our guide on **[how to block a subreddit](https://trykora.com/blog/how-to-block-a-subreddit/)** where we feature an awesome productivity tool called **[Kora](https://trykora.com)** that lets you block specific subreddits, block reddit entirely, or block all subreddits except the ones you find useful.

<iframe width="560" height="315" src="https://www.youtube.com/embed/BY7jicC6vfE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

## How to filter subreddits from /r/all on the new version of reddit (desktop)

If you're using the new version of reddit, filtering subreddits from /r/all is *a little bit trickier* than if you're using the old version of reddit. 

That's because the filtering options are not available on the new version of reddit. They're only displayed on the old desktop version of reddit. 

But don't worry! **You *can* still filter subreddits on the new version of reddit.**

You just **need to temporarily visit the old version to access the filtering options.**

### Step 1: login to reddit
The options to filter out subreddits are only presented when you're logged into a reddit account. Look to the top bar to login into an existing reddit account, or to create a new one.

![desktop_reddit_login_new](./desktop_reddit_login_new.png)

### Step 2: click the right arrow on the right hand side of your username
This will display a set of options.

![desktop_reddit_options](./desktop_reddit_options.png)

### Step 3: click *Visit Old Reddit*

Click **Visit Old Reddit** to visit the old version of reddit (where you'll find the subreddit filter options).

![desktop_reddit_new_visit_old_reddit](./desktop_reddit_new_visit_old_reddit.png)

Now that you're on the old version, **follow the steps below for filtering on the old version of reddit.**

After you've finished going through the steps below and have set up your filtering in the old version of reddit, type in *reddit.com* in the url address bar to return back to the new version of reddit.

## How to filter subreddits from /r/all on the old version of reddit (desktop)
If you're using the old desktop version of reddit you're in luck. The subreddit filtering feature was originally designed for old reddit and the settings are not yet displayed on the redesign. Right now accessing the feature is only available on the desktop version of old reddit. Here's how to do it:

### Step 1: login to reddit
The options to filter out subreddits are only presented when you're logged into a reddit account. Look to the sidebar to the right of the screen to login into an existing reddit account, or to create a new one.

![desktop_reddit_login](./desktop_reddit_login.png)

### Step 2: click "ALL" on the top
The filtering options are available under /r/all. To get there, click "ALL" on the top bar.

![desktop_reddit_old_all](./desktop_reddit_old_all.png)

### Step 3: look to the right sidebar and filter subreddits
In the right sidebar you'll be able to filter out any subreddits you want from /r/all by entering the subreddit's name and pressing the plus button to the right.

**ex: filtering /r/politics and /r/worldnews**

![desktop_reddit_old_filter_subreddits](./desktop_reddit_old_filter_subreddits.png)

**That's it! Refresh and the subreddits you added will no longer show up in your /r/all feed!**


## Another way to filter subreddits for desktop users: Reddit Enhancement Suite
Reddit Enhancement Suite is a powerful and popular set of tools packaged as a browser extension that modifies and *enhances* the reddit experience.

One of it's core features is filtering subreddits. Please note that right now **many of RES's features like filter subreddits don't work on the new version of reddit.** This is currently being worked on, but in the meantime **this method will only work if you're using the old version of reddit.**

You can install RES here: [Link](https://redditenhancementsuite.com/). Here's how to set up subreddit filtering:

### Step 1: open the RES settings console
After installing the RES extension, click the small gear icon on the top bar. This will display the RES dropdown. Click **RES settings console.**

![res_options](./res_options.png)

### Step 2: navigate to Subreddits
Look on the left sidebar of the settings console and click **Subreddits.**

![res_subreddits](./res_subreddits.png)

### Step 3: under filteReddit, scroll down to Subreddits
Add the subreddits you want to filter (one per line).

![res_subreddits](./res_filter.png)

### Step 4: check "/r/all, /r/popular and domain pages"
Check the third option to filter the subreddit from /r/all and /r/popular pages.

![res_subreddits_from](./res_subreddits_from.png)

### Step 5: save RES options
Click **save options** in the top bar to the right.

![res_save_options](./res_save_options.png)

**Now just refresh and you're all set!** The subreddits you filtered in RES will no longer show up in /r/all and /r/popular.




## How to filter subreddits from /r/all on the reddit app (mobile)
There is currently no default, in-app setting to filter subreddits on the native reddit app. Fortunately the official reddit help blog contains a verified workaround that is described step-by-step below:

***[According to the reddit help blog](https://www.reddithelp.com/en/categories/reddit-apps/reddit-app/how-do-i-filter-subreddits-native-apps):* if you filter a subreddit from /r/all on the desktop version of the site, you will no longer see that subreddit appear in /r/all in the native reddit apps.**

Fortunately you can easily access the desktop version of reddit from your mobile browser, and make all of the necessary changes right from your phone.


### Step 1: navigate to reddit mobile site
Navigate to **reddit.com** in your mobile browser. By default you will see reddit's mobile site.

![mobile_reddit_homepage](./mobile_reddit_homepage.PNG)

### Step 2: click options (the hamburger icon in the top right corner)
Click the hamburger icon to the top right to access reddit options.

![mobile_reddit_homepage_options](./mobile_reddit_homepage_options.png)

### Step 3: click "Log in / sign up" under options and log in / create your account
Click "Log in / sign up" and log into or create an account. The options to filter out subreddits are only presented when you're logged into a reddit account.

![mobile_reddit_login](./mobile_reddit_login.png)

### Step 4: click "Desktop Site" under options
After you've logged in, click "Desktop Site" under options to access the desktop reddit site (old version of reddit). The subreddit filtering options are only available on the old version of the desktop site.

![mobile_reddit_homepage_desktop_site](./mobile_reddit_homepage_desktop_site.png)

### Step 5: click "ALL" on the top
Once you're on the desktop version of old reddit, the filtering options are available under /r/all. To get there, zoom in and click "ALL" on the top bar.

![mobile_reddit_old_all](./mobile_reddit_old_all.png)

### Step 6: Scroll sideways to find the right sidebar and filter subreddits
In the right sidebar you'll be able to filter out any subreddits you want from /r/all by entering the subreddit's name and pressing the plus button to the right.

**ex: filtering /r/politics and /r/worldnews**

![mobile_reddit_old_filter_subreddits](./mobile_reddit_old_filter_subreddits.png)

### Step 7: Switch back to mobile reddit site
To get off the old desktop version of reddit and switch back to the default mobile reddit site, **scroll down to the bottom and click the "mobile website" link under "apps & tools."**

![mobile_reddit_switch_back](./mobile_reddit_switch_back.png)

This usually does the trick, but it's possible than an additional step will be required. **You may need to clear your mobile browsers cookies and browsing data to restore the mobile site.** 

This is browser specific, and a quick google search should point you in the right direction regarding how to do this on your mobile browser.

## Conclusion
We hope you found our guide on how to filter subreddits useful. We put a lot of time into taking what proves to be a very confusing process for reddit users and simplifying it as much as possible in an easy to follow, step-by-step way.

**If you want to go one step further and maximize your productivity, check out our guide on [how to block a subreddit](https://blog.trykora.com/how-to-block-a-subreddit)** where we feature **[Kora](https://trykora.com)**, a productivity tool that lets you block specific subreddits, block reddit entirely, or block all subreddits except the ones you find useful.


