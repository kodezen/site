---
title: How To Block Adult Sites In Google Chrome And Rewire Your Brain
description: This is a guide on how to block adult sites in Google Chrome
date: "2019-07-29T22:12:03.284Z"
---
So you ended up on the Hub again?

You know...that Hub.

The one where you enter like this:

![Jim](https://i.imgflip.com/bcfon.jpg?a434472)

And leave like this:

![Gob](https://media.giphy.com/media/IHLNydztb2Wwo/giphy.gif)

Right now you might be caught up in the cycle of the Hub. Vowing to never go back on, only to break your vow time and time again. 

You might feel beaten up and demoralized, after yet another failed attempt at staying off the Hub.

But there is hope.

Sometime in the last few days you might’ve realized that it’s not just about willpower. You might’ve realized that you need to beat the Hub with your tactics and intellect and not bang your head on the keyboard like a frustrated primate. 

Strategy is what wins wars, not brute force.

*The supreme art of war is to subdue the enemy without fighting* - Sun Tzu

While Sun Tzu is speaking of war, you should wonder if his methodology could be applied outside the context of war. Meaning can you beat the Hub without any effort at all?

In this post that’s exactly what we'll show you how to do. This is a battle plan to help you beat the Hub. If you implement the steps in here you’ll never end up there again. 

But make no mistake. I’m not offering you a golden egg. What I’m offering you is a fighting chance, a life preserver thrown off the side of a boat. You can grab it and hold on to it but you’ll still have to swim on your own. If you’re willing to swim, then read on.

## Step 1: Why Do You Go On The Hub?

When you end up on adult sites, a spark lights up in your brain and sends off a series of chemical reactions. This spark starts with a molecule called dopamine. It’s a neurotransmitter that your brain releases to make you seek rewards, meaning anything that makes you feel good. For millions of years this was a good system but recently it has started to break. 

![Dopamine](./dopamine.jpg)

If we rewind back to your ancestor a million years ago, what made him feel good was actually good for him. Your great great great great great great great grandpa would have gotten the occasional dopamine release to go seek out a piece of fruit or your prehistoric grandma.

Zoom forward to 2019 and things have changed a lot. We have high speed internet porn, genetically modified chicken nuggets, and disposable candy flavored nicotine sticks.

These advances of modern life, are causing your brain to “misfire”. Your brain thinks it's seeking something that's good for you.It doesn't know that your pursuing intangible digital mates and junk food with no nutritional value. 

**So how can we adjust for the modern age and prevent our brains from misfiring?**

Well it turns out that inside your brain exists a radar. This radar is constantly pinging your environment to see if rewards are nearby and easy to get. If this radar detects a convenient source of something that will make you feel good, it’ll send a message to your brain’s reward system to start releasing dopamine.

That’s why it’s *much harder* to resist eating a bag of potato chips when they’re already open in front of you. Or to refuse a drink while you’re already at the bar. 

By now the lightbulb should have went off in your head. You’ve realized that you have to make adult sites hard to get to. If they’re always a click away, you’re going to end up back on them. 

You have several options:

* Chrome extensions

Chrome extensions can block adult sites but they’re easy to remove and get around. At any point you can just disable the extension and get around it. Remember, the point is to have it so that adult sites are *really hard* to get to. 

* Parental Control Software 

There are several good options for parental control software out there that can block adult sites. However they are meant for parents to use on their kids computers. Not for someone to use on their own computer. This means that you’ll still be able to get around them whenever you want because you’re the one installing it. 

* Kora (Windows)

When we realized the problem with just extensions alone, we decided to build a tool called Kora. Kora is a much tougher desktop based web blocker lets you block sites and prevents you from bypassing it. 

You can [download it here](https://trykora.com).

After you install and setup Kora, you'll be taken to a screen where you can set up your first block. Here, simply add a list in Kora of the sites you would like to block. If you forget a few or encounter new ones don't worry, you can always edit your block and add in more sites later on.

[xxxBlocking](xxxblocking.png)
 
Next select the amount of time you want to block for. In this case I suggest you set the settings to as long as possible. Select start block so that all the sites you've entered are blocked from access.

Kora is unique as it has very strong security against blocks being bypassed. While a block is active, the blocks are enforced by Kora’s built in bypass prevention features. 

With this method, you no longer have to waste effort trying to subdue the enemy. You have used your tactics and intelligence to defeat your foe. 

* SelfControl (Mac)

We’re planning on developing Kora for Mac soon but for now there’s a program on it called SelfControl that works similarly. It’s not as powerful and lacks certain features but you can give it a try if you're on Mac.

## Step 3: Have You Enabled Google Safe Search?

Kora can block sites directly however it doesn’t prevent you from searching them into Google. You can however, turn on Google Safe Search so that no adult content will appear in any of your search results. This allows you to purge the locusts before they even enter your fields. Click the link below to get to your safe search settings. 
 
[Google Safe Search](https://www.google.com/preferences?hl=en&fg=1)

Set “Turn on safe search” to on.

![safesearch](./safesearch.png)

Then click “Lock safesearch” and log into your google account.

![locksafesearch](./locksafesearch.png)

Now safe search will be enabled on all google searches.

![locksafesearch](./locksafesearch.png)

## Step 4: Can Your Environment Condition You To Want To Go On The Hub?

Do you watch adult content in the same place every time? If so, you might've classically conditioned yourself. In his famous experiments, Russian scientist Ivan Pavlov was able to classically condition dogs to salivate at the sound of a bell.

It turns out we humans aren’t that much different than dogs. Right now you might be watching adult content in a specific place over and over again. Maybe your bedroom chair, your actual bed, or some other place in the home. 

When you do this, your environment starts to become a cue like the ding of a bell. You can condition your brain to expect adult content when you’re in that environment. This means if you’re always hanging out in your bedroom, using the laptop in bed, or just sitting indoors with nothing to do  you might’ve classically conditioned yourself to expect the rewards of adult sites. 

So break your conditioning.

This means GET OUT OF YOUR HOUSE. Go to the library, to the park, set up plans with friends, make friends if you don’t have any, volunteer to walk your neighbors dogs, mow your lawn and pull out weeds. Be creative and think of ways to be out of the house for most of your daily activities.

By doing this you can start to break your conditioning and allow your brain to rewire itself back to normal. 

## What Do I Do Next? 

*Every battle is won before it was fought* - Sun Tzu

Great generals don’t rush into battles like fools. They think and plan their victories in advance.

You may feel like a lowly soldier right now but with sufficient motivation and desire, you can become a great general. 

Think of this like a great battle. The battle in the saga of your life. You want to win, to vanquish the Hub from your life. A great victory like this means using the right tactics. In this article, we’ve shared with you several tactics that you can use to win the war. If you apply these tactics you’ll be on the path to victory.

We leave you with a few resources that will aid you on the path to victory. Good luck soldier.

Further reading:

Designing System To Augment Willpower:

[The Willpower Instinct](https://www.amazon.com/Willpower-Instinct-Self-Control-Works-Matters/dp/1583335080/ref=sr_1_4?keywords=willpower&qid=1564585891&s=gateway&sr=8-4)

[Willpower](https://www.amazon.com/Willpower-Rediscovering-Greatest-Human-Strength/dp/0143122231/ref=sr_1_3?keywords=willpower&qid=1564585905&s=gateway&sr=8-3)

The Neuroscience of Adult Sites:

[Your Brain On Porn](https://www.amazon.com/Your-Brain-Porn-Pornography-Addiction/dp/099316160X/ref=sr_1_1?keywords=your+brain+on+porn&qid=1564585989&s=gateway&sr=8-1)

How Sites Like The Hub Are Keeping You Hooked:

[Irresistible](https://www.amazon.com/Irresistible-Addictive-Technology-Business-Keeping-ebook/dp/B01HNJIK70/ref=sr_1_2?keywords=irresistible&qid=1564586220&s=gateway&sr=8-2)
