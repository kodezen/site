---
title: How to Block a Subreddit And Why You Should (Ultimate Guide)
description: This is a guide how to block a subreddit
date: "2019-07-28T22:12:03.284Z"
---

Have you ever found yourself browsing a specific subreddit for *waaaaaay* too long? 

Maybe /r/worldnews? /r/politics? 

Or /r/catmemes...?

![cat_meme](./cat_meme.jpg)

If you have, you're not alone. 

While its true that as reddit users we get tons of value from the website in the form of laughs, useful information, discussion with fellow humans, etc. it's also true that **sometimes our reddit use can stand in our way.**

If you're anything like me, you've **probably found it's shockingly easy to waste multiple hours on certain subreddits.**

Sometimes, reddit is just *too* good.

If you want to be more productive and have dreams and goals that matter to you, **this guide on how to block subreddits is for you.**

**Below you'll learn:**
* How to block a subreddit
* Why we waste time on subreddits
* Why we find it so hard to stop
* The hidden costs of wasting time on subreddits
* How to successfully stop wasting time on subreddits by blocking them

**The guide starts with a subreddit blocking tutorial, and is followed by a wealth of valuable insights that help you transform your social media habits and be more productive.**

**IMPORTANT NOTE**: this guide is for *blocking* subreddits. If you're looking to *filter* subreddits instead (hide them from /r/all feed) check out our guide on **[how to filter subreddits](https://trykora.com/blog/how-to-filter-subreddits/)**.

## How to block a subreddit
When we're on our computer, we're usually trying to accomplish important tasks like working, learning to code, writing an essay, research, learning graphic design, starting a business, etc. 

Maybe certain subreddits are distracting you and preventing you from being productive and focusing. 

**Below you'll find the the best options available for blocking a subreddit on your computer and how to set them up step-by-step.**

## How to block a subreddit on Windows
To block subreddits on Windows use **[Kora](https://trykora.com)**, a productivity tool designed to block distracting websites and help you focus.
[![Try Kora free](./kora_banner.png)](https://trykora.com)

### How to block a subreddit using Kora
If you prefer watching tutorial videos over step-by-step picture tutorials, we created a short video showing you how to block a subreddit on Windows using Kora

<iframe width="560" height="315" src="https://www.youtube.com/embed/-RbWTX5bHtM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Below you'll find a detailed step-by-step breakdown with pictures accompanying each step.

### Step 1: download and install Kora
To download Kora, click the link below and follow the onscreen installer instructions. The installation is fast and should only take a minute.

**[Download Kora Free](https://trykora.com)**

### Step 2: add the subreddits you want to block
Once you've installed Kora, you'll see the following screen. Here you can add all of the subreddits you want to block:

![kora_block_subreddits](./kora_block_subreddit.PNG)

### Step 3: choose to block now or schedule block
Kora lets you **create a simple one time block** or **create an automatically repeating schedule** for focus and productivity throughout the week.

#### Choose block type
![kora_choose_block_type](./kora_choose_block_type.PNG)

#### Block subreddits now
If you choose a simple one-time block starting now, you can set the block duration anywhere from 30 minutes all the way up to 7 days.

![kora_block_now](./kora_block_now.PNG)

#### Schedule subreddit block
If you choose so schedule your block, you can choose which days you want Kora to block on and the start and end times for each day.

**Block Days**

![kora_block_days](./kora_block_days.PNG)

**Block Start/End Times**

![kora_block_schedule](./kora_block_schedule.PNG)

### Step 4: name your block
Name your block so you can distinguish it from future blocks you create. I called mine "Distractions."

![kora_name_block](./kora_name_block.PNG)

### Step 5: enjoy focus and productivity
Now that you've created your block, you'll see a block page any time you try to go on the blocked subreddits encouraging you to focus. No more distractions!

![kora_blocked_subreddit](./kora_blocked_subreddit.png)

## How to block a subreddit on Mac
To block subreddits on Mac use **[Leechblock](https://www.proginosko.com/leechblock/)**, a browser extension that lets you block websites.

### How to block a subreddit using Leechblock
If you prefer watching tutorial videos over step-by-step picture tutorials, we created a short video showing you how to block a subreddit on Mac using Leechblock

<iframe width="560" height="315" src="https://www.youtube.com/embed/8SSHRI_xEXQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Step 1: install Leechblock
Visit the links below to add Leechblock to Chrome or Firefox.

**[Leeblock Chrome Extension](https://chrome.google.com/webstore/detail/leechblock-ng/blaaajhemilngeeffpbfkdjjoefldkok?hl=en-US)**

**[Leeblock Firefox Extension](https://addons.mozilla.org/en-US/firefox/addon/leechblock-ng/)**

### Step 2: left click the Leechblock extension icon and click options
Left clicking the Leechblock extension icon will show a dropdown. Click ***Options***.

![leechblock_options](./leechblock_options.png)

### Step 3: add the subreddits you want to block
Under the ***What to Block*** section, add the subbreddits you wish to block (one per line).

![leechblock_block_subreddits](./leechblock_block_subreddits.png)

### Step 4: set a block schedule
Under the ***When to Block*** section, enter the start and end times for the block (military time) and choose which days of the week you want to block on.

![leechblock_schedule](./leechblock_schedule.png)

### Step 5: change block page to website (optional)
Leechblock comes with some cool additional settings like being able to redirect to a specific website instead of just showing a block page. 

![leechblock_settings](./leechblock_settings.png)

### Step 6: save your options and enjoy your increased productivity
After saving your options at the bottom, try going on a subreddit you've blocked. Now you'll see a block page or will automatically be redirected to the site you set in your options. Enjoy the focus!

![leechblock_block_page](./leechblock_block_page.png)

## Why do we waste time on subreddits?

If you found this guide about blocking subreddits, you've probably noticed that you're wasting time on reddit and want to invest your time more wisely.

The first step in making change is understanding. It's asking: 

**why am I wasting time on subreddits in the first place?**

The answer lies in the realm of neuroscience. But don't worry. We're gonna skip all of the crazy technical explanations and keep the explanation at an easy-to-digest level. 

First up is **dopamine.**

## What is dopamine?

Dopamine is a neurotransmitter released in your brain. You can think of it as a chemical responsible for ***seeking, reward, and motivation***.

![dopamine](./dopamine.jpg)

In your brain you have several pathways in which dopamine plays a role, with one of the main ones being the **[Mesolimbic Dopamine Pathway](https://en.wikipedia.org/wiki/Mesolimbic_pathway)**.

This pathway is known as your brain's *reward pathway*: a neural pathway responsible for synthesizing rewarding experiences like **eating good food, making love, discovering funny or useful content online, etc.**

Your brain reinforces or forms new neural connections everytime you have a rewarding experience, which help it **remember and pursue sources of consistent rewards in the future.**

## How dopamine keeps us on subreddits

The first important thing to realize is that **reddit is essentially a never-ending stream of new information**. Because the content is user submitted, and because reddit gets more than 1.5 *BILLION* unique visits a month (statista), there's no end to the information and content posted to the site. There aren't enough hours in the day to consume all of it even if you wanted to.

<a href="https://www.statista.com/statistics/443332/reddit-monthly-visitors/" rel="nofollow"><img src="https://www.statista.com/graphic/1/443332/reddit-monthly-visitors.jpg" alt="Statistic: Combined desktop and mobile visits to Reddit.com from February 2018 to April 2019 (in millions) | Statista" style="width: 100%; height: auto !important; max-width:1000px;-ms-interpolation-mode: bicubic;"/></a>

The second key realization is that the majority of the information you stumble across on subreddits is **rewarding.** Because reddit works on a voting system, the content you see is content that other users deemed was worthy of creating, sharing, and discussing. 

**The rewards can come in many forms: a laugh after seeing a meme, arousal at something cool that was posted, excitement after reading a self-improvement post, etc.**

The ***reward system in our brains*** is activated any time we come across novel, rewarding content online. Our brain begins to associate the site with more and more rewarding experiences until **we eventually start going on the site mindlessly out of habit, influenced by our brain's desire for pleasurable and rewarding experiences.**

## What is oxytocin?
Oxytocin is a neurotrasmitter released in your brain. You can think of it as a chemical responsible for ***trust, love, and bonding.***

![oxytocin](./oxytocin.jpg)

It is released during physical touch, ***social interactions***, and is thought to play a major role in empathy and the forming of connections with fellow humans.

## How oxytocin keeps us on subreddits

At our core, we are social animals. Not only are we highly influenced by the behaviors of those around us, but we crave belonging and actively seek out social connection. *Love and Belonging* is on Maslow's Hierarchy of Needs.

**This innate desire to connect with others draws us to reddit and other forms of social media.** Most of us see social media as social interaction, but *a simulated, abstract form of it.*

We have an intuitive understanding that while we interact with other human beings when we go on a subreddit, the multiple levels of abstraction make this kind of human interaction a very different experience compated to the more intimate, face-to-face social interactions our ancestors had.

This point that the digital social experiences we're having today are unlike any we've had before was touched on in a recent article by Wired titled **["Learn From These Bugs. Don't Let Social Media Zombify You."](https://www.wired.com/story/learn-from-these-bugs-dont-let-social-media-zombify-you/)** UCLA psychologist Patricia Greenfield was quoted as saying:
> "It was clearly adaptive to be so sensitive to social stimuli. But evolution never expected that we would be getting social stimuli from people we don't even know."

![social_media](./social_media.jpg)

The shocking and counter-intuitive thing is that this artificial, abstract form of social interaction **affects us in much the same way that real world bonding experiences do.**

A recent FastCompany article titled **["Social Networking Affects Brains Like Falling in Love"](https://www.fastcompany.com/1659062/social-networking-affects-brains-falling-love)** described a fascinating experiment involvoing social media conducted by Dr. Paul J. Zak, a leading expert on how oxytocin influences human behavior.

The author of the article served as the test subject, and has his blood drawn before and after a 10 minute tweeting session. 

**The process of tweeting caused the test subject's oxytocin levels to increase 13.2%**, which was the equivalent of the oxytocin level change Dr. Zak had measured in ***a groom during his wedding ceremony***.

Dr. Zak's commentary on the results:

> "Your brain interpreted tweeting as if you were directly interacting with people you cared about or had empathy for. E-connection is processed in the brain like an in-person connection."

![brain](./brain.png)

## Coincidence or engineering?
The fact that social media seems to perfectly synch up with our biological hardwiring seems like the perfect coincidence for the social media giants profiting off it's rise. **But is it really a coincidence?**

Former Facebook president Sean Parker has spoken out and suggested otherwise:

> "The thought process that went into building these applications, Facebook being the first of them, ... was all about: 'How do we consume as much of your time and conscious attention as possible?'" 

> "And that means that we need to sort of give you a little dopamine hit every once in a while, because someone liked or commented on a photo or a post or whatever. And that's going to get you to contribute more content, and that's going to get you ... more likes and comments." 

> "It's a social-validation feedback loop ... exactly the kind of thing that a hacker like myself would come up with, because you're exploiting a vulnerability in human psychology." **"The inventors, creators — it's me, it's Mark [Zuckerberg], it's Kevin Systrom on Instagram, it's all of these people — understood this consciously. And we did it anyway."** 

Source: [Axios](https://www.axios.com/sean-parker-unloads-on-facebook-god-only-knows-what-its-doing-to-our-childrens-brains-1513306792-f855e7b4-4e99-4d60-8d51-2775559c2671.html)

<iframe width="560" height="315" src="https://www.youtube.com/embed/LPwR1i-sWpo?start=49" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## The hidden costs of wasting time on subreddits
Now that you have a good understanding of the underlying forces shaping your behavior, it's time to generate **sufficient motivation to change.**

The best way to do that is to look at all of the hidden cost that your current reddit habits may be having on your life.

#### Time cost
The first is the **time cost**. Just an hour a day wasted scrolling through feeds leads to **15 wasted days** over the course of a year.


#### Opportunity cost
The second is the **opportunity cost**. That's *15 days* a year that could be invested in the goals that matter most to you.


#### Motivation and discipline cost
The third is the negative effects on your **motivation and discipline.** The reason most of us find it so hard to do the hard things we know we should is because our environments are not optimized for the delayed-gratification pursuits that matter to us. 

We’re surrounded by sources of artificial pleasure and instant gratification that are much more alluring than going to the gym, learning how to code, or reading a book.

It will always be much easier to scroll through reddit than it will be to put in the effort necessary to realize your dreams.

![motivation](./motivation.jpg)


## How to stop wasting time on subreddits by blocking them

The key to effectively stop wasting time on subreddits is to acknowledge your biological hardwiring and **rely on tools rather than willpower.** 

Tristan Harris, former product manager at Google, was recently quoted in the infamous **["Our Minds Have Been Hijacked by Our Phones"](https://www.wired.com/story/our-minds-have-been-hijacked-by-our-phones-tristan-harris-wants-to-rescue-them/)** Wired article.

> It’s essential to understand that we experience the world through a mind and a meat-suit body operating on evolutionary hardware that’s millions of years of old, and that **we’re up against thousands of engineers and the most personalized data on exactly how we work on the other end.**

Social media giants are continually running experiments, testing out new features that slowly erode our agency in the pursuit of a supposed enhanced user experience. Infinite never-ending newsfeeds and auto-playing videos are two examples. 

The algorithms only get better and better with time. Relying on willpower alone is not a viable long-term strategy. **We believe it's much smarter to use mindful technology that's been developed to solve this problem.**

The war on our attention was one of our motivators for developing **[Kora](https://trykora.com)**, a productivity tool that empowers our users and gives them back control. **Our software lets you block  distracting websites on the internet** so that you can focus and spend your time on the things that matter most to you.

[![Try Kora free](./kora_banner.png)](https://trykora.com)

**[Try Kora free](https://trykora.com)**

## Conclusion
In this guide you've learned how to block a subreddit, the reasons why you should, and the many underlying factors influencing your social media habits.

We hope this guide has been of service to you, and hope you use the information contained within to make positive changes in your life.

Till next time!










