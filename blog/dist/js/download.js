document.addEventListener('DOMContentLoaded', () => {
    console.log(navigator.platform);
    let isWin = (navigator.platform.includes('Win'));
    if (!isWin) {
        let downloadContainer = document.getElementsByClassName("download-container")[0];
        if (downloadContainer) {
            downloadContainer.classList.add('none');
        }
        let bottomDownload = document.getElementsByClassName("bottom-download")[0];
        if (bottomDownload) {
            bottomDownload.classList.add('none');
        }
        let windowsOnlyList = document.getElementsByClassName("windows-only");
        for (var i = 0; i < windowsOnlyList.length; i++) {
            let windowsOnly = windowsOnlyList[i];
            windowsOnly.classList.remove('none');
        }
    }
});